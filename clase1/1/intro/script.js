function printMessage(name) {
    return "Nice to meet you, " + name;
}
var user = "Angular Dev";
// Using a variable - message
var message = printMessage(user);
document.getElementById("container").innerHTML = message;
// Same line
//document.getElementById("container").innerHTML = printMessage(user);
