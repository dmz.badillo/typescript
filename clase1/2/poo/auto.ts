class Auto
{
	public color :    string;
	public modelo:    string;
	public velocidad: number;

	public getColor()
	{
		return this.color;
	}
	public setColor(color: string)
	{
		this.color = color;
	}
}

var auto  = new Auto();
var auto2 = new Auto();
var auto3 = new Auto();

auto.setColor('Azul');
auto2.setColor('Rojo');
auto3.setColor('Negro');

console.log( "El color del auto  es: " + auto.getColor() );
console.log( "El color del auto2 es: " + auto2.getColor() );
console.log( "El color del auto3 es: " + auto3.getColor() );
