var Auto = /** @class */ (function () {
    function Auto() {
    }
    Auto.prototype.getColor = function () {
        return this.color;
    };
    Auto.prototype.setColor = function (color) {
        this.color = color;
    };
    return Auto;
}());
var auto = new Auto();
var auto2 = new Auto();
var auto3 = new Auto();
auto.setColor('Azul');
auto2.setColor('Rojo');
auto3.setColor('Negro');
console.log("El color del auto  es: " + auto.getColor());
console.log("El color del auto2 es: " + auto2.getColor());
console.log("El color del auto3 es: " + auto3.getColor());
